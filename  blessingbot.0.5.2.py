#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Simple Bot to process blessings
This program is NOT dedicated to the public domain under the any license.
"""
from __future__ import print_function
import os
os.environ["NLS_LANG"] = "RUSSIAN_RUSSIA.AL32UTF8"
import sys
from threading import Thread

import logging
import telegram
from telegram import MessageEntity
from telegram.error import NetworkError, Unauthorized
from time import sleep
from datetime import datetime
from datetime import timedelta
import string
import db_config


#import cx_Oracle
#import pymysql
import MySQLdb

#connection = cx_Oracle.connect('BLESSINGBOT/XoFXgxS1TB4Ni63nwL1N@db201910070839_medium')
#cursor = connection.cursor()

#update_id = None

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, Filters,MessageHandler,MessageHandler
import time

TOKEN = "XXXXXXXXX:ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"

logging.basicConfig(filename='blessingbot.log',format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

#class MyConnection(.Connection):
#
#    def __init__(self):
#        #print("Connecting to database")
#        return super(MyConnection, self).__init__(db_config.user, db_config.pw, db_config.dsn)
#class MyConnection:
def MyConnection():
    con=MySQLdb.connect("localhost",db_config.user,db_config.pw,db_config.dbname,3306,use_unicode=True, charset="utf8mb4")
    #cmd=con.cursor()
    return con
    #super(MySQLdb, self).__init__("127.0.0.1",db_config.user,db_config.pw,db_config.dbname,3306)
#obj=Connection()

#cur = connection.cursor()

#class Connection():
#	def __init__(self):
#		#self.pool = cx_Oracle.SessionPool(user, pw, tns, min=2, max=20, increment=1, encoding="UTF-8")
#                self.connection = cx_Oracle.SessionPool('BLESSINGBOT','XoFXgxS1TB4Ni63nwL1N', 'db201910070839_medium', increment=1, encoding="UTF-8")
#                self.connection.acuire()
#                return self.connection

#class Connection(cx_Oracle.Connection):
#
#    def __init__(self):
#        connectString = "BLESSINGBOT/XoFXgxS1TB4Ni63nwL1N@db201910070839_medium"
#        print("CONNECT to database")
#        return super(Connection, self).__init__(connectString)
#
#    def cursor(self):
#        return Cursor(self)


# sample subclassed cursor which overrides the execute() and fetchone()
# methods in order to perform some simple logging
#class Cursor(cx_Oracle.Cursor):
#
#    def execute(self, statement, args):
#        print("EXECUTE", statement)
#        print("ARGS:")
#        for argIndex, arg in enumerate(args):
#            print("   ", argIndex + 1, "=>", repr(arg))
#        return super(Cursor, self).execute(statement, args)
#
#    def fetchone(self):
#        print("FETCH ONE")
#        return super(Cursor, self).fetchone()
#    def fetchmany(self):
#        print("FETCH ONE")
#        return super(Cursor, self).fetchmany()



def main():
    """Run the bot."""
    global update_id
    global chat_id
    global updater
    updater = Updater(TOKEN, use_context=True)
    bot = updater.bot
    dp = updater.dispatcher
    start_handler = CommandHandler('start', register)
    dp.add_handler(start_handler)
    dp.add_handler(MessageHandler(Filters.status_update | Filters.entity(MessageEntity.URL) | Filters.entity(MessageEntity.TEXT_LINK) | Filters.forwarded, vaporize,pass_user_data=True,pass_chat_data=True)) 
    dp.add_handler(MessageHandler(Filters.text | Filters.photo, sanitize,pass_user_data=True,pass_chat_data=True)) 
    dp.add_handler(CallbackQueryHandler(removebutton))
    updater.job_queue.run_repeating(nwalkthrough,2)
    updater.job_queue.run_repeating(owalkthrough,600)
    updater.start_polling(timeout=15, read_latency=4)
    dp.add_handler(CallbackQueryHandler(button))
    updater.idle()

def sanitize(update,context):
    #print(dir(update.message))
    print(sanitize)
    if update.message.reply_to_message:
        reply_to = update.message.reply_to_message
    else:
        reply_to = None
    bot = context.bot
    connection = MyConnection()
    print(connection)

    cursor = connection.cursor()
    reply_to = update.message.reply_to_message
    zchat_id=update.message.chat_id
    chk2sql = "select * from BUSERS where status = 'banned' and userid = %s" % (zchat_id)
    cursor.execute(chk2sql)
    rchk2sql = cursor.fetchone()
    print(rchk2sql)
    if not rchk2sql:
        if not reply_to:
            chat_id=update.message.chat_id
            message_id=update.message.message_id
            sdate=(update.message.date+timedelta(seconds=86400)).strftime("%Y-%m-%d %H:%M:%S")
            text=update.message.text
            #print("HERE")
            askForBlessing(update,bot)
            print("HERE!")
            print(update.message)
            sql = "insert into BMSG (omessage_id,createmsgdate,deletemsgdate,ochat_id,msgtxt,status) values (%s,LOCALTIMESTAMP(),'%s',%s,'%s','new')" % (message_id,sdate,chat_id,text)
            print(sql)
            try:
                cursor.execute(sql)
            except:
                print("failed to process: ",sql)
            connection.commit()

        else:
            ochat_id=update.message.chat_id
            reply_to_message_id=update.message.reply_to_message.message_id
            reply_to_chat_id=update.message.reply_to_message.chat_id
            omessage_id=update.message.message_id
            sdate=update.message.date
            text=update.message.text
            reply_to_text=update.message.reply_to_message.text
            username,message_text=reply_to_text.split(":")
            
            if text.strip() == "!ban":
                print("'"+text.strip()+"'")
                a=context.bot.send_message(ochat_id,'мы примем меры')
                #chksql = "SELECT nchat_id,nmessage_id  FROM BMSG where dbms_lob.substr( msgtxt , length(msgtxt), 1 )='%s'" % (reply_to_text)
                chksql = "SELECT ochat_id,omessage_id  FROM BMSG where status='new' and msgtxt = '%s'" % (message_text.strip())
                print(chksql)
                cursor.execute(chksql)
                rchksql = cursor.fetchall()
                tdate=(update.message.date+timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S")
                print(rchksql)
                if rchksql:
                    for each in rchksql:
                        nchat_id=each[0]
                        nmessage_id=each[1]
                        msgupdate = "update BMSG set deletemsgdate='%s' where status='new' and omessage_id = %s and ochat_id=%s" % (tdate,each[1],each[0])
                        cursor.execute(msgupdate)
                        print(msgupdate)
                        connection.commit()
                    
                    chk1sql = "select * from BUSERS where userid = %s" % (nchat_id)
                    cursor.execute(chk1sql)
                    rchk1sql = cursor.fetchone()
                    if not rchk1sql:
                        sql = "insert into BUSERS (userid,chat_id,status) values (%s,%s,'banned')" % (nchat_id,nmessage_id)
                        cursor.execute(sql)
                        connection.commit()
                        print('user %s banned by insertion' % nchat_id)
                    else:
                        sql = "update BUSERS set status='banned' where userid=%s" % (nchat_id)
                        cursor.execute(sql)
                        connection.commit()
                        print('user %s banned by update' % nchat_id)
                bot.delete_message(ochat_id,omessage_id)
            if text.strip() == "!del":
                print("'"+text.strip()+"'")
                a=context.bot.send_message(ochat_id,'мы примем меры')
                #chksql = "SELECT nchat_id,nmessage_id  FROM BMSG where dbms_lob.substr( msgtxt , length(msgtxt), 1 )='%s'" % (reply_to_text)
                chksql = "SELECT ochat_id,omessage_id  FROM BMSG where status='new' and msgtxt='%s'" % (message_text.strip())
                print(chksql)
                cursor.execute(chksql)
                rchksql = cursor.fetchall()
                tdate=(update.message.date+timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S")
                print(rchksql)
                if rchksql:
                    for each in rchksql:
                        nchat_id=each[0]
                        nmessage_id=each[1]
                        msgupdate = "update BMSG set deletemsgdate='%s' where status='new' and omessage_id = %s and ochat_id=%s" % (tdate,each[1],each[0])
                        cursor.execute(msgupdate)
                        print(msgupdate)
                        connection.commit()
                    
                bot.delete_message(ochat_id,omessage_id)
        if text.strip() == "!srestart":
                print("'"+text.strip()+"'")
                a=context.bot.send_message(ochat_id,'полный перезапуск')
                bot.delete_message(ochat_id,omessage_id)
                restart(update)


    else:
        a=context.bot.send_message(ochat_id,'вы в бане')
    connection.close()


    
def vaporize(update,context):
    #try:
    connection = MyConnection()
    cursor = connection.cursor()
    chat_id=update.message.chat_id
    username=update.effective_message.from_user.username
    print(update.effective_user.id)
    print(update.effective_chat.id)
    print(update.effective_message.from_user.id)
    userid=update.effective_message.from_user.id
    sql = "SELECT * FROM BUSERS where userid=%s and acl=2" % (userid)
    print(sql)

    cursor.execute(sql)
    row = cursor.fetchone()
    if not row: 

        try:
            pinned_message_id=update.effective_message.pinned_message.message_id
        except:
            pinned_message_id=-1
        message_id=update.message.message_id
        bot=context.bot
        #bot.delete_message('blessme_bot',481)

        try:
            if not update.message.pinned_message:
                print("here!!!")
                bot.delete_message(chat_id,message_id)
            else:
                #pinned = shelve.open("pinned")
                #pinned[str(chat_id)]=str(pinned_message_id)
                #pinned.close()
                msgsql = "insert into BMSG (omessage_id,ochat_id,msgtxt,status) values (%s,%s,%s,'pinned')" % (pinned_message_id,chat_id,update.effective_message.pinned_message.text)
                cursor.execute(msgsql)
                connection.commit()

        except:
            bot.delete_message(chat_id,message_id)
    else:
        print("Admin can do")
        print(username)
    connection.close()

def owalkthrough(context):
    bot = context.bot
    connection = MyConnection()
    cursor = connection.cursor()

    msgsql = "SELECT ochat_id,omessage_id,deletemsgdate FROM BMSG where status='new' and nmessage_id is null and date(deletemsgdate)<LOCALTIMESTAMP()"

    cursor.execute(msgsql)

    rows = cursor.fetchall()
    for each in rows:
        chat_id=each[0]
        message_id=each[1]
        if int(message_id)>3:
            msgsql = "update BMSG set status = 'deleted' where status='new' and nmessage_id is null and omessage_id = %s and ochat_id=%s" % (message_id,chat_id)
            try:
                bot.delete_message(chat_id,message_id)
                cursor.execute(msgsql)
                connection.commit()
            except:
                cursor = connection.cursor()
                cursor.execute(msgsql)
                connection.commit()

    connection.close()

def nwalkthrough(context):
    bot = context.bot
    connection = MyConnection()

    cursor = connection.cursor()
    msgsql = "SELECT nchat_id,nmessage_id,deletemsgdate FROM BMSG where status='new' and nmessage_id is not null and date(deletemsgdate)<LOCALTIMESTAMP()"

    cursor.execute(msgsql)
    rows = cursor.fetchall()
    for each in rows:
        chat_id=each[0]
        message_id=each[1]
        a=(each[2]<datetime.now())
        if int(message_id)>3:
            msgsql = "update BMSG set status = 'deleted' where status='new' and nmessage_id = %s and nchat_id=%s" % (message_id,chat_id)
            try:
                bot.delete_message(chat_id,message_id)

                cursor.execute(msgsql)
                connection.commit()
            except:
                cursor = connection.cursor()
                cursor.execute(msgsql)
                connection.commit()

    connection.close()

def cleanup(context):
    bot = context.bot
    maxi = shelve.open("maxid")
    lst=list(maxi.keys())
    pinned=shelve.open("pinned")
    print("cleanup started")
    for each in lst:
        try:
            pinned_message_id=pinned[each]
        except:
            pinned_message_id=-1
        cntr = int(maxi[each])
        ecntr=0
        while cntr>1:
            if ecntr < 100:
                if not pinned_message_id==cntr:
                    print(cntr)
                    try:
                        bot.delete_message(each,cntr)
                    except:
                        ecntr+=1
                        print("empty")
            cntr-=1

    print("cleanup finished")
    maxi.close()
    pinned.close()


def button(update, context):
    query = update.callback_query
    keyboard = [[InlineKeyboardButton("🙏", callback_data='1')]]
    reply_markup=telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True,resize_keyboard=True)
    query.edit_message_reply_markup(None)


def register(update, context):
    print("!!!!!!!")
    connection = MyConnection()

    cursor = connection.cursor()
    blessers_id = str(update.message.from_user.id)
    blessers_username = update.message.from_user.username
    a=context.bot.send_message(str(blessers_id),'Jai Guru Dev!')
    chat_id=a.chat.id
    message_id=a.message_id
    command_message_id=update.message.message_id
    command_chat_id=update.message.chat_id
    command_date=(update.message.date+timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S")
    msginsert = "insert into BMSG (ochat_id,omessage_id,deletemsgdate,msgtxt) values (%s,%s,'%s','%s')" % (command_chat_id,command_message_id,command_date,update.message.text)
    cursor.execute(msginsert)
    connection.commit()

    zdate=(a.date+timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S")
    msginsert = "insert into BMSG (ochat_id,omessage_id,deletemsgdate,msgtxt) values (%s,%s,'%s','%s')" % (chat_id,message_id,zdate,update.message.text)
    cursor.execute(msginsert)
    connection.commit()

    alreadysql = "SELECT * FROM BUSERS where username='%s'" % (blessers_username)
    insertsql = "insert into BUSERS (userid,username,createdate,chat_id,acl,status) values (%s,'%s',LOCALTIMESTAMP(),%s,1,'enabled')" % (blessers_id,blessers_username,chat_id) 
    
    cursor.execute(alreadysql)
    already = cursor.fetchone()
    if not already:
        cursor.execute(insertsql)
        connection.commit()
        print("registered")
        context.bot.send_photo(chat_id=blessers_id, photo=open('Guruji.jpg', 'rb'))
        b=context.bot.send_message(chat_id=blessers_id, text="""Благодарим за регистрацию в Блессинг боте, в дальнейшем вы здесь увидите запросы на блессинг.  Как только Вы всех благославите нажав на 🙏, и удалите все сообщения, то появится кнопка Старт. Нажимать ее снова не нужно. Как только появится новый запрос, она исчезнет.  Вы можете удалить все сообщения с инструкциями и оставить фото Гуруджи, тогда кнопка Старт не будет появляеться. 

Если вы сами хотите отправить запрос на Блессинг, это можно сделать прямо в боте в  строке ввода. Если она не активна - нажмите Старт.  Запрос увидят только блессеры в боте.""")
        chat_id=b.chat.id
        message_id=b.message_id
        zdate=(b.date+timedelta(seconds=600)).strftime("%Y-%m-%d %H:%M:%S")

        msginsert = "insert into BMSG (ochat_id,omessage_id,deletemsgdate,msgtxt) values (%s,%s,'%s','%s')" % (chat_id,message_id,zdate,update.message.text)
        cursor.execute(msginsert)
        connection.commit()
    else:
        b=context.bot.send_message(chat_id=blessers_id, text="уже зарегистрировались :]")
        chat_id=b.chat.id
        message_id=b.message_id
        zdate=(b.date+timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S")

        msginsert = "insert into BMSG (ochat_id,omessage_id,deletemsgdate,msgtxt) values (%s,%s,'%s','%s')" % (chat_id,message_id,zdate,update.message.text)
        cursor.execute(msginsert)
        connection.commit()

    connection.close()


def askForBlessing(update,bot):
    connection = MyConnection()
    cursor = connection.cursor()
    if not update.message.pinned_message:
        first_name=update.message.from_user.first_name
        last_name=update.message.from_user.last_name
        chat_id=update.message.chat_id
        if first_name is None:
            first_name=""
        if last_name is None:
            last_name=""
        username=update.message.from_user.username
        text=update.message.text
        try:
            photo=update.message.photo[-1].file_id
        except:
            photo=None

        caption=update.message.caption
        if caption is None:
            caption=""
        else:
            caption=": " + caption

        sql = "SELECT * FROM BUSERS where status='enabled'"
        
        cursor.execute(sql)
        rows = cursor.fetchall()

        for each in rows:
            username=each[0]
            usernickname=each[1]

            keyboard = [[InlineKeyboardButton("🙏", callback_data='removebutton')]]
            reply_markup = InlineKeyboardMarkup(keyboard)
            try:
                if photo!=None and caption!=None and text!=None:
                    bot.send_photo(username, photo, caption=str(first_name + " " + last_name + caption),disable_notification=True)
                    ds=bot.send_message(username,str(first_name + " " + last_name + ": " + text),reply_markup=reply_markup,disable_notification=True)
                elif photo!=None and caption!=None and text==None:
                    ds=bot.send_photo(username, photo, caption=str(first_name + " " + last_name + caption),reply_markup=reply_markup,disable_notification=True)
                elif photo==None and caption==None and text!=None:
                    ds=bot.send_message(username,str(first_name + " " + last_name + ": " + text),reply_markup=reply_markup,disable_notification=True)
                elif photo!=None and caption==None and text==None:
                    ds=bot.send_photo(username, photo, caption=str(first_name + " " + last_name + caption),reply_markup=reply_markup,disable_notification=True)
                else:
                    ds=bot.send_message(username,str(first_name + " " + last_name + ": " + text),reply_markup=reply_markup,disable_notification=True)
                sent_message_id=ds.message_id

                tdate=(ds.date+timedelta(seconds=86400)).strftime("%Y-%m-%d %H:%M:%S")
                print(datetime.now())
                print(ds)
                print(tdate)
                print(datetime.now()+timedelta(seconds=86400))
                sql = "insert into BMSG (omessage_id,nmessage_id,createmsgdate,deletemsgdate,ochat_id,nchat_id,msgtxt,status) values (%s,%s,LOCALTIMESTAMP(),'%s',%s,%s,'%s','new')" % (update.message.message_id,sent_message_id,tdate,chat_id,username,text)

                cursor.execute(sql)
                connection.commit()
    


            except Unauthorized as e:
                print(e)
                print('username:',username)
                print('usernickname:',usernickname)

    connection.close()

def removebutton(update, context):
    connection = MyConnection()

    cursor = connection.cursor()
    query = update.callback_query
    sent_message_id=update.effective_message.message_id
    username=update.effective_message.chat_id
    tdate=(update.effective_message.date+timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S")
    print(tdate)
    msgupdate = "update BMSG set deletemsgdate='%s' where nmessage_id = %s and nchat_id=%s" % (tdate,sent_message_id,username)
    cursor.execute(msgupdate)
    connection.commit()
    connection.close()

def stop_and_restart():
    """Gracefully stop the Updater and replace the current process with a new one"""
    updater.stop()
    print("stopping and restarting...")
    os.execl(sys.executable, sys.executable, *sys.argv)

def restart(update):
#    update.message.reply_text('Bot is restarting...')
    print("restarting")
    Thread(target=stop_and_restart).start()

def error_callback(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)


if __name__ == '__main__':
    main()
